import java.util.HashSet;
import java.util.Set;

public class task5110 {
    public static void main(String[] args) throws Exception {
        task5110 task = new task5110();

        //stask1
        String input = "Devcamp JAVA exercise";
        task5110.task1(input);

        //stask 2
        String ipst21 = "word";
        String ipst22 = "drow";
        task5110.task2(ipst21, ipst22);

        //stask 3
        String ipst3 = "Devcamp";
        String ipst31 = "HaHa";
        String resultT3 = findFirstUniqueCharacter(ipst3);
        System.out.println("Ket qua cua chuoi " + ipst3 + " la: " + resultT3);
        String resultT31 = findFirstUniqueCharacter(ipst31);
        System.out.println("Ket qua cua chuoi " + ipst31 + " la: " + resultT31);

        //stask 4
        String ipst4 = "word";
        String resultT4 = reverseString(ipst4);
        System.out.println("Dao nguoc cua chuoi " + ipst4 + " la: " + resultT4);

        //stask 5
        String ip5 = "123asd";
        boolean resultT5 = containsNoDigits(ip5);
        System.out.println("Kiem tra co so nao trong chuoi " + ip5 + " nay khong: " + resultT5);

        //stask 6
        String ip6 = "java";
        task.countCharVowel(ip6);

        //stask 7
        String numberString = "1234";
        int number = Integer.parseInt(numberString);
        System.out.println(number);

        //stask 8
        String strT8 = "devcamp java";
        strT8 = strT8.replace('a', 'b');
        System.out.println(strT8);

        //stask 9
        String strT9 = "I am developer";
        String[] words = strT9.split(" ");
        StringBuilder reversedStr = new StringBuilder();
        for (int i = words.length - 1; i >= 0; i--) {
            reversedStr.append(words[i]);
            if (i != 0) {
                reversedStr.append(" ");
            }
        }
        System.out.println("Chuoi sau khi dao nguoc: " + reversedStr.toString());

        //stask 10
        String strT10 = "aba";
        task.checkReverge(strT10);
    }
    //stask 1
    public static void task1(String s){
        String lowerCaseInput = s.toLowerCase();
        Set<Character> duplicates = new HashSet<>();
        for (char c : lowerCaseInput.toCharArray()) {
            if (lowerCaseInput.indexOf(c) != lowerCaseInput.lastIndexOf(c)) {
                duplicates.add(c);
            }
        }
        if (duplicates.isEmpty()) {
            System.out.println("NO");
        } else {
            for (char c : duplicates) {
                System.out.print("'" + c + "' ");
            }
        }
    }
    //stask 2
    public static void task2(String s1, String s2){
        if (s1.length() != s2.length()) {
            System.out.println("KO");
        } else {
            boolean isReverse = true;
            for (int i = 0; i < s1.length(); i++) {
                if (s1.charAt(i) != s2.charAt(s2.length() - 1 - i)) {
                    isReverse = false;
                    break;
                }
            }
            if (isReverse) {
                System.out.println("OK");
            } else {
                System.out.println("KO");
            }
        }
    }
    //stask 3
    public static String findFirstUniqueCharacter(String input) {
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (input.indexOf(c) == input.lastIndexOf(c)) {
                return Character.toString(c);
            }
        }
        return "NO";
    }
    //stask 4
    public static String reverseString(String input) {
        StringBuilder sb = new StringBuilder(input);
        return sb.reverse().toString();
    }
    //stask 5
    public static boolean containsNoDigits(String input) {
        boolean checkNumber = false;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (Character.isDigit(c)) {
                checkNumber = true;
            }
        }
        return checkNumber;
    }
    //stask 6 đếm co bao nhieu nguyen am
    public static void countCharVowel(String input){
        int count = 0;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
                count++;
            }
        }
        System.out.println("Chuoi co " + count + " nguyen am.");
    }
    //stask 10
    public static void checkReverge(String str){
        boolean isPalindrome = true;
        for (int i = 0; i < str.length() / 2; i++) {
            if (str.charAt(i) != str.charAt(str.length() - i - 1)) {
                isPalindrome = false;
                break;
            }
        }
        System.out.println("Kiem tra chuoi co dao nguoc khong: " + isPalindrome);
    }
}
